class CreateSubject < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.integer :code
      t.string :name

      t.timestamps
    end
  end
end
