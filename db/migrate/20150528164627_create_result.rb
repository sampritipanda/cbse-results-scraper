class CreateResult < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.belongs_to :student
      t.belongs_to :subject
      t.string :grade
      t.integer :theory
      t.integer :practical
      t.integer :total

      t.timestamps
    end
  end
end
