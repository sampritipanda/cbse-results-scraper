class CreateStudent < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.integer :roll_no
      t.string :name

      t.timestamps
    end
  end
end
