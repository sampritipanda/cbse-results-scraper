class Subject < ActiveRecord::Base
  has_many :results

  validates_uniqueness_of :code
  validates_presence_of :code
end

class Student < ActiveRecord::Base
  has_many :results

  validates_uniqueness_of :roll_no
  validates_presence_of :roll_no, :name
end

class Result < ActiveRecord::Base
  belongs_to :student
  belongs_to :subject

  validates_presence_of :student, :subject, :theory, :practical, :total
end
