require 'bundler/setup'
Bundler.require

ActiveRecord::Base.establish_connection(
  adapter:  'postgresql',
  pool: 20,
  encoding: 'unicode',
  database: 'cbse_results',
  username: 'postgres'
)

require './models.rb'

class String
  def digits_only
    gsub(/\D/, '')
  end

  def letters_only
    gsub(/[^\w\s]/, '')
  end

  def is_i?
    not digits_only == ""
  end
end
