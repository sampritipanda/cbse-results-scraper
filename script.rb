# Results Scraper

require './config.rb'

def fetch(roll)
  HTTParty.post("http://cbseresults.nic.in/class12/cbse122015_all.asp", body: { regno: roll }, headers: { "Referer" => "http://cbseresults.nic.in/class12/cbse122015_all.htm"}).body
end

def check(roll)
  response = fetch(roll)
  not (response.include? "Result Not Found" or response.include? "Invalid Roll No")
end

start_prefix = 10
start_suffix = 0

unless Student.count == 0
  max_code = Student.maximum(:roll_no).to_s
  start_prefix = max_code[0..1].to_i
  start_suffix = max_code[2..-1].to_i
  student = Student.find_by!(roll_no: max_code)
  student.results.destroy_all
  student.destroy!
end

(start_prefix..99).each do |prefix|
  next unless check("#{prefix}00000") ||
    check("#{prefix}00001") ||
    check("#{prefix}00002") ||
    check("#{prefix}00003") ||
    check("#{prefix}00004") ||
    check("#{prefix}00005")

  (start_suffix..99999).each do |suffix|
    roll = "#{prefix}#{"%05d" % suffix}"
    response = fetch(roll)
    next if response.include? "Result Not Found" or response.include? "Invalid Roll No"

    puts roll

    obj = Nokogiri::HTML(response)
    rows = obj.css("body > div:nth-child(7) > div > center > table tr")

    name = obj.css("body > div:nth-child(7) > table:nth-child(2) tr:nth-of-type(2) > td:nth-of-type(2)").text.strip.letters_only
    student = Student.create!(roll_no: roll, name: name)

    rows.each do |r|
      cells = r.css('td')
      next unless cells[0].text.is_i?

      subj = Subject.find_or_create_by(code: cells[0].text.digits_only.to_i)
      Result.create!(student: student, subject: subj, theory: cells[2].text.digits_only.to_i, practical: cells[3].text.digits_only.to_i, total: cells[4].text.digits_only.to_i, grade: cells[5].text.strip.letters_only)
    end
  end
end
